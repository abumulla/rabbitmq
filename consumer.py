import os
import sys
import pika

def callback(ch, method, properties, body):
        print(f" [x] Message: {(body)}")
        
def main():
    remote_host = '13.233.94.114'  # Replace with the hostname or IP address of your remote server
    port = 5672  # Default RabbitMQ port, update if different
    virtual_host = '/'  # Default virtual host, update if different
    username = 'abumulla'  # Replace with your RabbitMQ username
    password = 'abu123'  # Replace with your RabbitMQ password

    # Create a connection to the remote RabbitMQ server
    credentials = pika.PlainCredentials(username, password)
    connection_parameters = pika.ConnectionParameters(remote_host, port, virtual_host, credentials,heartbeat=1000,connection_attempts=3,retry_delay=5,socket_timeout=10)

    connection = pika.BlockingConnection(connection_parameters)
    # connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()

    channel.queue_declare(queue='hello-queue')

    channel.basic_consume(queue='hello-queue',
                        auto_ack=True,
                        on_message_callback=callback)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)