import pika

remote_host = '13.233.94.114'  # Replace with the hostname or IP address of your remote server
port = 5672  # Default RabbitMQ port, update if different
virtual_host = '/'  # Default virtual host, update if different
username = 'abumulla'  # Replace with your RabbitMQ username
password = 'abu123'  # Replace with your RabbitMQ password

# Create a connection to the remote RabbitMQ server
credentials = pika.PlainCredentials(username, password)
connection_parameters = pika.ConnectionParameters(remote_host, port, virtual_host, credentials,heartbeat=1000,connection_attempts=3,retry_delay=5,socket_timeout=10)

connection = pika.BlockingConnection(connection_parameters)
# connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()


channel.queue_declare(queue='hello-queue')
flag = True
while flag:
    msg = input("Message: ")
    if msg == "exit":
        flag = False
        break
    channel.basic_publish(exchange='',
                        routing_key='hello-queue',
                        body=msg)
    print(" [x] Message Sent!")

connection.close()

